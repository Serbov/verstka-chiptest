'use strict';

var gulp = require('gulp');

// web server
var connect = require('gulp-connect');

gulp.task('webserver', function () {
    connect.server({
        root: './',
        port: 9000,
        livereload: true
    });
});

//reload
gulp.task('html:reload', function () {
    gulp.src('./*.html')
        .pipe(connect.reload());
});

// sass transformer
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function () {
    return gulp.src('./styles/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./styles'));
});

// watchers
gulp.task('sass:watch', function () {
    gulp.watch('./styles/**/*.scss', ['sass']);
});

gulp.task('css:watch', function () {
    gulp.watch('./styles/**/*.css', ['html:reload']);
});

gulp.task('html:watch', function () {
    gulp.watch('./*.html', ['html:reload']);
});

gulp.task('js:watch', function () {
    gulp.watch('./js/**/*.js', ['html:reload']);
});

gulp.task('watcher', ['sass:watch', 'css:watch', 'html:watch', 'js:watch']);

gulp.task('default', ['webserver', 'sass', 'watcher']);

